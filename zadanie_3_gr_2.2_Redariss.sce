//Matlab applications in physics
//Author: Radoslaw Paluch
//Technical Physics

//To use the programme it's script must be in the same directory as folder "data"
//working directory must also be the same as the location of the script

fileRead= list(); //list of files insides
files=listfiles("data"); //list of filenames

//variables used to find maximum of PM10
maksimum = 0;
maksimumIndex = 0;
day=0;

//lists of PM10 data at night and day
Night=list();
Day=list();

//loop going through each file 
for n = 1:size(files)(1)
    //reading file content
    fileRead(n) = csvRead("data\"+files(n),";",",","string",[",","."]);
    //loking for max in PM10 column
    [tempMaks,tempIndex]=max(strtod(fileRead(n)(2:25,6)));
    //checking if maximum is the highest in whole data set
    if tempMaks > maksimum then
        maksimum = tempMaks;
        maksimumIndex = tempIndex;
        day = n;
    end
    //dawn and dusk hours from http://slonce.alternatywne.info/kalendarz/10_2019.html
    //I rounded dawn hour to 7:00 and dusk hour to 18:00
    
    //reading data measured at night
    Night(n)=(strtod(fileRead(n)([2:8,19:25],6)));
    //reading data measured at day
    Day(n)=(strtod(fileRead(n)([9:18],6)));
end

//calculating average PM10 concentration during day and  night in whole data set
avNightAll=mean(list2vec(Night));
avDayAll=mean(list2vec(Day));

//performing statistical test
nom=abs(avNightAll-avDayAll);
s1=stdev(list2vec(Night))/sqrt(length(list2vec(Night)));
s2=stdev(list2vec(Day))/sqrt(length(list2vec(Day)));
den=sqrt(s1^2+s2^2);
t=nom/den;
// obtainig hour of maximum concetration of PM10
mHour=fileRead(day)(maksimumIndex+1,1);
// obtainig date from filename
mDate=strsplit(files(day),["_","."])(2);
//obtainig date from first filename
period1=strsplit(files(n),["_","."])(2);
//obtainig date from last filename
period2=strsplit(files(1),["_","."])(2);

//checking if averages are statistically diffrent displaying info in main scilab window  and writing to file "PMresults.dat"
if t<2 then
    //displaying in main window, ascii(10) used to end line
    mprintf("there is no statistical diffrence between night and day concentration of PM10, test gave t= %f "+ascii(10),t);
    com1=msprintf(", test gave t= %f ",t);//creating a formated string
    //creating file and writing in it
    print("PMresults.dat","there is no statistical diffrence between night and day concentration of PM10"+com1+ascii(10));
elseif t>=2 & t<3 then
    mprintf("there is no significant statistical diffrence between night and day concentration of PM10, test gave t= %f "+ascii(10),t);
    com1=msprintf(", test gave t= %f ",t);
    print("PMresults.dat","there is no significant statistical diffrence between night and day concentration of PM10"+com1+ascii(10));
else
    mprintf("there is a statistical diffrence between night and day concentration of PM10, test gave t= %f "+ascii(10),t);
    com1=msprintf(", test gave t= %f ",t);
    print("PMresults.dat","there is a statistical diffrence between night and day concentration of PM10"+com1+ascii(10));
end



//displaying results in main scilab window and writnig them to file "PMresults.dat"
mprintf("Maximum concetration of PM10 amounted to %d and occured on %s at %s",maksimum,mDate,mHour);
//creating a formated string
com2=msprintf('Maximum PM10 concentration was %d and occured at %s on %s',maksimum, mHour, mDate);
com3=msprintf("//analized period since %s to %s",period1,period2);
fd=mopen("PMresults.dat",['a']);//openig file for adding data
mfprintf(fd,'%s',com2+"  "+com3);
mclose(fd);

